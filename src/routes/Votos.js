const express = require('express')
let router = express.Router(),
  log = require('../logger/Logger').getLogger(__filename),
  SqlServer = require('tedious-wrapper-felipe-as'),
  sqlServerParams = {
    poolSize: 10,
    queryTimeout: 3000,
    userName: 'bolsoneverson',
    password: 'XP@azx01',
    url: 'localhost'
  }

router.get('/', async (req, res) => {
  try {
    let sql = 'Select is_facista facista from pessoa_voto'
    let data = await new SqlServer(sqlServerParams).query(sql)

    return res.send(data.reduce((acc, atual) => {
      atual.facista === true ? acc[0] += 1 : acc[1] += 1
      return acc
    }, [0, 0]))
  } catch (err) {
    log.error(`Erro ao listar pessoas\n${err}`)
    return res.status(400).json({ error: ':(' })
  }
})

router.post('/', async (req, res) => {
  try {
    let nome = req.body.nome
    let facista = req.body.facista

    let sql = 'Insert into pessoa_voto(nome, is_facista) VALUES(@nome, @is_facista)'
    await new SqlServer(sqlServerParams).query(sql, [[ { nome: 'nome', tipo: 'varchar', valor: nome }, { nome: 'is_facista', tipo: 'bit', valor: facista } ]])

    return res.send({ ok: 'votado' })
  } catch (err) {
    log.error(`Erro ao inserir voto`)
    log.error(err)
    return res.status(400).json({ error: 'Você já votou' })
  }
})

module.exports = router
