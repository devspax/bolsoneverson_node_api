const express = require('express')
let router = express.Router(),
  log = require('../logger/Logger').getLogger(__filename),
  SqlServer = require('tedious-wrapper-felipe-as')
  sqlServerParams = {
    poolSize: 10,
    queryTimeout: 3000,
    userName: 'bolsoneverson',
    password: 'XP@azx01',
    url: 'localhost'
  }

router.get('/', async (req, res) => {
  try {
    let sql = 'Select nome from pessoa_voto'
    let result = await new SqlServer(sqlServerParams).query(sql)
    return res.send(result)
  } catch (err) {
    log.error(`Erro ao listar pessoas\n${err}`)
    return res.status(400).json({ error: ':(' })
  }
})

router.post('/', async (req, res) => {
  try {
    let nome = req.body.nome
    let facista = req.body.facista

    let sql = 'Insert into pessoa_voto(nome, is_facista) VALUES(@nome, @is_facista)'
    await new SqlServer(sqlServerParams).query(sql, [{ name: 'nome', type: 'VarChar', value: nome }, { name: 'is_facista', type: 'Bit', value: facista }])

    return res.send({ ok: 'votado' })
  } catch (err) {
    log.error(`Erro ao inserir voto\n${err}`)
    return res.status(400).json({ error: 'Você já votou' })
  }
})

module.exports = router
