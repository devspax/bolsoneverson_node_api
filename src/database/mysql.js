'use-strict'
const mysql = require('mysql')
  pool = mysql.createPool({
    connectionLimit: process.env.MYSQL_CONNECTIONLIMIT,
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    port: process.env.MYSQL_PORT,
    multipleStatements: true
  })

const getConn = () => new Promise((resolve, reject) => {
  pool.getConnection((err, connection) => {
    if (err) {
      reject(err)
    } else {
      resolve(connection)
    }
  })
})

const queryComParam = (conn, sql, params) => new Promise((resolve, reject) => {
  conn.query(sql, params, (err, data) => {
    conn.release()
    if (err) {
      reject(err)
    } else {
      resolve(data)
    }
  })
})

const semParam = (conn, sql) => new Promise((resolve, reject) => {
  conn.query(sql, (err, data) => {
    conn.release()
    if (err) {
      reject(err)
    } else {
      resolve(data)
    }
  })
})

const query = async (sql, params) => {
  try {
    let conn = await getConn()
    return params
      ? queryComParam(conn, sql, params)
      : semParam(conn, sql)
  } catch (err) {
    throw err
  }
}

// const query = (sql, params) => new Promise((resolve, reject) => {
//   getConn()
//   .then(conn => {
//     return params 
//       ? queryComParam(conn, sql, params) 
//       : semParam(conn, sql)
//   })
//   .then(data => resolve(data))
//   .catch(err => reject(err))
// })

module.exports = { query }
