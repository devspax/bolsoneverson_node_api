const { createLogger, format, transports } = require('winston')
const { combine, timestamp, label, printf } = format

const myFormat = printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`
})

module.exports = {
  getLogger: (filename) => {
    let config = {
      format: combine(
        label({ label: filename }),
        timestamp(),
        myFormat
      ),
      transports: [
        new transports.Console(),
        new transports.File({ filename: 'logs/app.log' }),
        new transports.File({ filename: 'logs/error.log', level: 'error' })
      ]
    }
    return createLogger(config)
  }
}
