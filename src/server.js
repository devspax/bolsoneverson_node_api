const express = require('express'),
  log = require('./logger/Logger').getLogger(__filename),
  bodyParser = require('body-parser'),
  cors = require('cors'),
  ping = require('./routes/Ping'),
  vote = require('./routes/Vote'),
  votos = require('./routes/Votos')

const start = () => {
  let app = express() // Instancia do express

  // Parâmetros de requisição
  app.use(cors())
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use('/back/ping', ping)
  app.use('/back/vote', vote)
  app.use('/back/votos', votos)

  let PORT = process.env.PORT || '9091'
  let HOST = process.env.HOST || '0.0.0.0'
  app.listen(PORT, HOST, (err) => {
    if (err) {
      log.error('Erro ao inicializar o server!')
      log.error(err)
      process.exit(1)
    }

    log.info(`Server funcionando em ${HOST}:${PORT}`)
  })
}

module.exports = {
  start
}
