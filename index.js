const log = require('./src/logger/Logger.js')
  .getLogger(__filename),
  server = require('./src/server')

const init = () => {
  log.info('Inicializando o app')
  server.start()
}

process.on('unhandledRejection', (reason, p) => log.error(`Unhandled Rejection at: ${p}\nreason: ${reason}`))
process.on('uncaughtException', (error) => log.error(`Uncaught exception\n${error}`))

init()
