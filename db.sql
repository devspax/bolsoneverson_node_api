CREATE USER 'bolsoneverson' identified by 'paxazx01';
GRANT ALL PRIVILEGES ON *.* TO 'bolsoneverson'@'localhost' WITH GRANT OPTION;
flush privileges;

CREATE DATABASE bolsoneverson;
USE bolsoneverson;

CREATE TABLE IF NOT EXISTS pessoa_voto(
	id int unsigned primary key auto_increment,
    nome varchar(255) not null unique,
    is_facista boolean
);
